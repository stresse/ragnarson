﻿#if false
using System.Collections.Generic;
using System.Linq;
using wManager.Wow.ObjectManager;
using robotManager.Helpful;
#endif


namespace Library
{
    public static string Test = "this is a fucking test, morron";
    public class NPC
    {
        public string Name;
        public int Entry;
        public Vector3 Position;
        public string ZoneName;
        public string Category;
        public int GossipOption;
//        static NPC() { }
        public NPC(string name, int entry, Vector3 position, string zoneName, string category, int gossipOption = 1)
        {
            Name = name;
            Entry = entry;
            Position = position;
            ZoneName = zoneName;
            Category = category;
            GossipOption = gossipOption;
        }
    }
    public class NPCFinder
    {
        public static NPC Serrah = new NPC("Serr'ah", 115287, new Vector3(-789.6111f, 4391.22f, 740.1577f, "None"), "Dalaran", "PetHealer");
        public static NPC BodhiSunwayver = new NPC("Bodhi Sunwayver", 99210, new Vector3(-710.0521f, 4644.417f, 720.7136f,"None"), "Dalaran", "PetTamer");
        public List<NPC> NPCs = new List<NPC>() { Serrah, BodhiSunwayver,};
        public NPC Nearest { get { return NPCs.OrderBy(h=> h.Position.DistanceTo(ObjectManager.Me.Position)).FirstOrDefault(); } }
        public NPC NearestPetHealer { get { return NPCs.Where(h => h.Category == "PetHealer").OrderBy(h=> h.Position.DistanceTo(ObjectManager.Me.Position)).FirstOrDefault(); } }
        public NPC NearestPetTamer { get { return NPCs.Where(h => h.Category == "PetTamer").OrderBy(h => h.Position.DistanceTo(ObjectManager.Me.Position)).FirstOrDefault(); ; } }
        //  Type-Search Template
        //public NPC NearestTYPE { get { return NPCs.Where(h => h.Category == "TYPE").OrderBy(h => h.Position.DistanceTo(ObjectManager.Me.Position)).FirstOrDefault(); ; } }
    }
}
