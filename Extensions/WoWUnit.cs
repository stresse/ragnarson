﻿using System;
using System.Collections.Generic;
using System.Linq;
using robotManager.Helpful;
using System.Text;
using wManager.Wow;
using wManager.Wow.ObjectManager;
using wManager.Wow.Helpers;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using System.Reflection;

public static partial class Extensions
{
    //private const string WoWUnitInvalid = "Parameter passed as WoWUnit is not a valid unit (probably null)?";
    public static Spell CurrentSpell => ObjectManager.Me.CastingSpell;
    public static bool HasDebuffType(this WoWUnit unit, string type)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        //  NON WORKING CODE     ---     NOTE TO SELF: NEED TO IMPLEMENT THIS        --      NON WORKING CODE
        //  ^^ I'm actually not sure what that's about...I know this is terrible code maintenance...bad me
        return unit.HaveBuff(type);
    }
    public static bool HasDebuff(this WoWUnit unit, string name)
    {
        if (unit is WoWUnit u)
            return u.HaveBuff(name);
        throw new ArgumentException("Parameter passed as WoWUnit is not a valid unit (probably null)?");
    }

    public static bool Casting(this WoWUnit unit)
    {
        if (unit is WoWUnit u)
            return u.IsCast;
        throw new ArgumentException("Parameter passed as WoWUnit is not a valid unit (probably null)?");
    }

    public static bool CastingSpellInList(this WoWLocalPlayer me, params string[] names)
    {
        if (me == null)
            throw new ArgumentNullException(nameof(me));
        return names.Any(name => name == me.CastingSpell.Name || name == CurrentSpell.Name);
    }
    public static bool HasAnyDebuff(this WoWUnit unit, params string[] names)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        return names.Any(name => unit.HaveBuff(name));
    }

    public static bool HasAllBuffs(this WoWUnit unit, params string[] names)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        IEnumerable<WoWUnit> Corpses = wManager.Wow.ObjectManager.ObjectManager.GetObjectWoWUnit().Where(u => u != null && u.Type == wManager.Wow.Enums.WoWObjectType.Corpse);
        foreach (string name in names)
        {
            if (!unit.HaveBuff(name))
            {
                return false;
            }
        }
        return true;
    }
    public static bool InLoS(this WoWUnit unit)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));

        return !TraceLine.TraceLineGo(ObjectManager.Me.Position, unit.Position, CGWorldFrameHitFlags.HitTestLOS);
    }
    /*public static bool IsInRange(this WoWUnit unit)
    {
        return unit.IsInRange;
    }
    */
    public static bool IsJoke(this WoWUnit unit)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        uint slaveLevel = ObjectManager.Me.Level;
        uint enemyLevel = unit.Level;
        uint levelDifference = slaveLevel - enemyLevel;
        if (levelDifference > 20)
        {
            return true;
        }
        return false;
    }
    public static bool IsPlayer(this WoWUnit unit)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        return unit.Type == wManager.Wow.Enums.WoWObjectType.Player; // Player = 6
    }
    public static bool IsVillainOrJoke(this WoWUnit unit)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        return (unit.IsJoke() || unit.IsVillain());
    }
    public static bool IsVillain(this WoWUnit unit)
    {
        if (unit == null)
            throw new ArgumentNullException(nameof(unit));
        return unit.IsTargetingMeOrMyPetOrPartyMember;
    }


    /*
     * How do I check if a corpse is lootable??
     * public static string Owner(this WoWCorpse unit)
    {
        if (unit.Type == wManager.Wow.Enums.WoWObjectType.Corpse) { return };
            //Memory.WowMemory.Memory.ReadUInt64(unit.GetDescriptorAddress(Descriptors.CorpseFields.Owner));
    }
    */

}
