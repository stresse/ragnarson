﻿using System;
using System.Collections.Generic;
using System.Linq;
using robotManager.Helpful;
using System.Text;
using wManager.Wow;
using wManager.Wow.ObjectManager;
using wManager.Wow.Helpers;
using wManager.Wow.Class;

namespace Tools
{ }
//  Trash to cannibalize (if there is anything of worth) and discard
/*
        public class Logger
    {
        static Log(){ }
        //public object Inputs { get; set; }
        private static List<object> Inputs;
        public static string Output;
        public string TextToPrint { get; set; }
        public string ProjectName { get; set; } // [System.Runtime.CompilerServices.CallerMemberName] ;
        public Log(List<object> inputs)
        {
            //  Inputs = new List<object>{ inputs } ;

        }
        public void Initialize(params object[] inputs)
        {
            Inputs = new List<object> { inputs } ;

        }
        public void WriteBotLog(string logValue)
        {
            Logging.Write(TextToPrint);
        }
        public void WriteWowLog(string logValue)
        {
            Lua.LuaDoString(string.Format(@"DEFAULT_CHAT_FRAME:AddMessage(""|cff008000{0}|r :{1}"")", ProjectName, TextToPrint)) ;
        }
        public static string GetLogValue(object input)
        {
            try
            {
                return input.ToString() ; //.GetMethod("ToString") != null;
            }
            catch
            {
                Logging.Write("ERROR: Stresse's custom logger fucked something up.") ;
                return "ERROR" ;
            }
        }
    }
}
*/
//  End Trash
public static partial class Extensions
{
    public static bool HasMethod(this object objectToCheck, string methodName)
    {
        Type type = objectToCheck.GetType();
        return type.GetMethod(methodName) != null;
    }
    /*
    public static string FormatLua(string str, params object[] names)
    {
        return string.Format(str, names.Select(s => s.ToString().Replace("'", "\\'").Replace("\"", "\\\"")).ToArray());
    }
    */
    public static string FormatLua(this string str, params object[] names)
    {
        return string.Format(str, names.Select(s => s.ToString().Replace("'", "\\'").Replace("\"", "\\\"")).ToArray());
    }

    private static string LuaAndCondition(string[] names, string varname)
    {
        StringBuilder sb = new StringBuilder();
        foreach (string name in names)
        {
            sb.Append(" and " + varname + " == \"" + name + "\"");
        }
        return sb.ToString().Substring(5);
    }

    private static string LuaOrCondition(string[] names, string varname)
    {
        StringBuilder sb = new StringBuilder();
        foreach (string name in names)
        {
            sb.Append(" or " + varname + " == \"" + name + "\"");
        }
        return sb.ToString().Substring(4);
    }

    private static string LuaTable(string[] names)
    {
        string returnValue = "{";
        foreach (string name in names)
        {
            returnValue += "[\"" + name + "\"] = false,";
        }
        return returnValue += "};";
    }

    public static void DeleteItems(string itemName, int leaveAmount)
    {
        int itemQuantity = ItemsManager.GetItemCountByNameLUA(itemName) - leaveAmount;
        if (string.IsNullOrWhiteSpace(itemName) || itemQuantity <= 0)
        {
            return;
        }

        Logging.WriteFight("Cleaning up:" + itemName);

        string execute =
            "local itemCount = " + itemQuantity + "; " +
            "local deleted = 0; " +
            "for b=0,4 do " +
            "if GetBagName(b) then " +
            "for s=1, GetContainerNumSlots(b) do " +
            "local itemLink = GetContainerItemLink(b, s) " +
            "if itemLink then " +
            "local _, stackCount = GetContainerItemInfo(b, s)\t " +
            "local leftItems = itemCount - deleted; " +
            "if string.find(itemLink, \"" + itemName + "\") and leftItems > 0 then " +
            "if stackCount <= 1 then " +
            "PickupContainerItem(b, s); " +
            "DeleteCursorItem(); " +
            "deleted = deleted + 1; " +
            "else " +
            "if (leftItems > stackCount) then " +
            "SplitContainerItem(b, s, stackCount); " +
            "DeleteCursorItem(); " +
            "deleted = deleted + stackCount; " +
            "else " +
            "SplitContainerItem(b, s, leftItems); " +
            "DeleteCursorItem(); " +
            "deleted = deleted + leftItems; " +
            "end " +
            "end " +
            "end " +
            "end " +
            "end " +
            "end " +
            "end; ";
        Lua.LuaDoString(execute);
    }
}
