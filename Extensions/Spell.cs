﻿using System;
using System.Collections.Generic;
using System.Linq;
using robotManager.Helpful;
using System.Text;
using wManager.Wow;
using wManager.Wow.ObjectManager;
using wManager.Wow.Helpers;
using wManager.Wow.Class;

public static partial class Extensions
{
    public static readonly List<string> AoESpells = new List<string>(){
        "Halo",
        "Holy Nova",
        "Power Word: Barrier"
    };
    /*
     *public static float Cooldown(this wManager.Wow.Class.Spell spell)
        {
            return spell.Cooldown();
        }
    */
    public static bool CastSpell(this Spell spell, WoWUnit unit, bool force = false)
    {
        Logging.Write("Casting spell " + spell.NameInGame + " on " + unit.Name);
        //	Handle bullshit units
        //  Note To Self: Do I want to handle "unit.IsDead" differently? Should I let that case pass to "force" parameter as 
        if (unit == null || !unit.IsValid || unit.IsDead || !spell.KnownSpell || !spell.IsSpellUsable)
        {
            return false;
        }

        /*	Check If Toon is Wanding
        if (spell.Name == "Shoot" && IsAutoRepeating("Shoot"))
        {
            return true;
        }
        */

        //	Fuck off if toon is already casting and (force == false)
        if (ObjectManager.Me.IsCast && !force)
        {
            Logging.Write("Fucking off");
            return false;
        }
        //	Stop moving if spell needs to be cast
        if (spell.CastTime > 0)
        {
            MovementManager.StopMoveTo(false, Usefuls.Latency + 500);
        }
        //	Force cast spell if (force == true)
        if (force)
        {
            Lua.LuaDoString("SpellStopCasting();");
        }
        //	Cast AoE spells by location
        if (AoESpells.Contains(spell.Name))
        {
            Lua.LuaDoString("CastSpellByName(\"" + spell.NameInGame + "\")");
            ClickOnTerrain.Pulse(unit.Position);
        }
        else
        {
            //	Face unit to cast
            if (!unit.IsLocalPlayer)
            {
                Logging.WriteDebug("Turning to face unit.");
                MovementManager.Face(unit);
                //MovementManager.
            }
            if (unit.IsLocalPlayer)
            {
                SpellManager.CastSpellByNameOn(spell.NameInGame, unit.Name);
            }
            if (unit.IsMyTarget)
            {
                SpellManager.CastSpellByNameOn(spell.NameInGame, "target");
            }
            else
            {
                MemoryRobot.Int128 currentMousePosition = ObjectManager.Me.MouseOverGuid;
                ObjectManager.Me.MouseOverGuid = unit.Guid;
                SpellManager.CastSpellByNameOn(spell.NameInGame, "mouseover");
                ObjectManager.Me.MouseOverGuid = currentMousePosition;
                Logging.Write(string.Format(@"Mouseover cast spell ""{0}"" on ""{1}"" (spell=""{0}"", unit=""{1}"", guid=""{2}"")", spell.NameInGame, unit.Name, unit.Guid));
            }
        }
        return true;
    }

}
