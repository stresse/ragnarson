using System;
using robotManager.FiniteStateMachine;
using robotManager.Helpful;
using wManager.Wow.Class;
using wManager.Wow.Helpers;
using wManager.Wow.Helpers.FightClassCreator;
using wManager;
using System.Linq;

public static class Effects
{
    public const string Buff    = "buff";
    public const string Debuff  = "debuff";
    public const string Damage  = "damage";
    public const string Heal    = "heal";
    public const string Protect = "protect";
}

public static class Relations
{
    public const string Ally = "ally";
    public const string Enemy = "enemy";
}

public static class Sources
{
    public const string Spellbook = "spellbook";
    public const string ExtraButton = "extrabutton";
    public const string Item = "item";

}

public static class Mediums
{
    public const string Cast = "cast";
    public const string Channel = "channel";
}

public static class Kinds
{
    
}

public static class TargetSelection
{

}
public enum TargetCategory
{
    Enemy = 1,
    Friendly,
    GameObject,
    Terrain
}

public class RotationAction
{
    string effect;
    Spell action;
}

enum ActionMediums
{

}

namespace wManager.Wow.Bot.States
{
    public class RotationStep : State
    {
        private bool canMoveDuringCast;
        private string castOn;
        private bool checkAlive;
        private bool checkCombat;
        private bool checkKnown;
        private bool checkLoS;
        private bool checkMounted;
        private bool checkRange;
        private bool checkUsable;
        private Delegate condition;
        private bool debug;
        private string description;
        private bool faceTarget;
        private bool forceRun;
        private bool forceTarget;
        private bool isAoE;
        private bool isAoEMePos;
        private bool lockFrame;
        private string name;
        private bool notSpellIsCSharpCode;
        private bool notSpellIsLuaBotCode;
        private bool notSpellIsLuaScript;
        private bool notSpellIsVBCode;
        private bool oncePerTarget;
        private Spell spell;
        private bool targetFriends;
        private int timer;
        private string spellName;

        public RotationStep
        (
            string actionName,
            int priority,
            Delegate condition,

            // ACTION INFO
            bool isAoE = false,
            bool isBuff = false,

            // EFFECTS
            //Aura aura = FightClassSpellsDB FightClassSpellsDB.

            
            // REQUIREMENTS
            bool checkAlive = true,
            bool checkCombat = true,
            bool checkKnown = true,
            bool checkLoS = true,
            bool checkMounted = true,
            bool checkRange = true,
            bool checkUsable = true,

            // TARGET MANAGEMENT
            bool faceTarget = false,

            // OVERRIDES
            bool forceTarget = false,
            bool forceRun = false,

            // ADDITIONAL MODIFIERS
            bool stopMovement = false
        )
        {

        }
        public string Name { get => name; private set => name = value; }
        public string Description { get => description; private set => description = value; }
        public Delegate Condition { get => condition; private set => condition = value; }
        public override string DisplayName => "Spell " + spellName;
        public bool IsAoE { get => isAoE; private set => isAoE = value; }
        public string NameAndDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(description))
                    return spellName;
                return string.Format("{0} ({1})", spellName, description);
            }
        }

        public override bool NeedToRun
        {
            get
            {
                try
                {
                    if (!Conditions.InGameAndConnected)
                        return false;

                    if (lockFrame)
                        Memory.WowMemory.LockFrame();

                    int time = 0;

                    if (debug)
                        time = Environment.TickCount;
                    bool r = NeedToRunPrivate();
                    if (debug)
                        Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun time to execute: " + (Environment.TickCount - time));
                    return r;
                }
                catch (Exception e)
                {
                    Logging.WriteError("RotationStep > NeedToRun: " + e);
                }
                finally
                {
                    if (lockFrame)
                        Memory.WowMemory.UnlockFrame();
                }
                return false;
            }
        }

        public bool NotSpellIsCSharpCode { get => notSpellIsCSharpCode; private set => notSpellIsCSharpCode = value; }
        public bool NotSpellIsLuaBotCode { get => notSpellIsLuaBotCode; private set => notSpellIsLuaBotCode = value; }
        public bool NotSpellIsLuaScript  { get => notSpellIsLuaScript; private set => notSpellIsLuaScript = value; }
        public bool NotSpellIsVBCode { get => notSpellIsVBCode; private set => notSpellIsVBCode = value; }
        public sealed override int Priority { get; set; }
        public Spell Spell { get => spell; private set => spell = value; }
        public string SpellName { get => spellName; private set => spellName = value; }
        public bool CheckMounted { get => checkMounted; private set => checkMounted = value; }
        public bool CheckAlive { get => checkAlive; set => checkAlive = value; }
        public bool CheckCombat { get => checkCombat; set => checkCombat = value; }
        public bool CheckKnown { get => checkKnown; set => checkKnown = value; }
        public bool CheckLoS { get => checkLoS; set => checkLoS = value; }
        public bool CheckRange { get => checkRange; set => checkRange = value; }
        public bool CheckUsable { get => checkUsable; set => checkUsable = value; }
        public bool FaceTarget { get => faceTarget; set => faceTarget = value; }
        public bool ForceRun { get => forceRun; set => forceRun = value; }
        public bool ForceTarget { get => forceTarget; set => forceTarget = value; }

        private bool NeedToRunPrivate()
        {
            if (!Conditions.InGameAndConnected)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - Usefuls.InGame = " + Usefuls.InGame + ", Usefuls.IsLoadingOrConnecting = " + Usefuls.IsLoadingOrConnecting + ", ObjectManager.ObjectManager.Me.IsValid = " + ObjectManager.ObjectManager.Me.IsValid);
                return false;
            }

            if (robotManager.Products.Products.InPause)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - robotManager.Products.Products.InPause");
                return false;
            }

            if (ObjectManager.ObjectManager.Me.IsOnTaxi)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - ObjectManager.ObjectManager.Me.IsOnTaxi");
                return false;
            }

            if ((!notSpellIsLuaScript && !notSpellIsCSharpCode && !notSpellIsVBCode && !notSpellIsLuaBotCode) && (spell == null || spell.Id <= 0))
                return false;

            if (timer > 0)
            {
                if (!timerClass.IsReady)
                {
                    if (debug)
                        Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - timer");
                    return false;
                }
            }

            if (ObjectManager.ObjectManager.Me.IsInPetBattle)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - IsInPetBattle");
                return false;
            }

            if (ObjectManager.ObjectManager.Me.IsDeadMe && !castIfMeDead)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - castDead");
                return false;
            }

            if (oncePerTarget && ObjectManager.ObjectManager.Target.IsValid && oncePerTargetLastGuid == ObjectManager.ObjectManager.Target.Guid)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - oncePerTarget");
                return false;
            }

            if (ObjectManager.ObjectManager.Me.IsMounted && !castIfInMount)
            {
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - castMounted");
                return false;
            }

            if (!targetFriends)
            {
                if (inCombatOnly && (!Fight.InFight || !ObjectManager.ObjectManager.Target.IsValid))
                {
                    if (debug)
                        Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - inCombatOnly");
                    return false;
                }

                if (needInView && !isBuff && ObjectManager.ObjectManager.Target.IsValid)
                {
                    if (wManagerSetting.CurrentSetting.CalcuCombatRange)
                    {
                        if (TraceLine.TraceLineGo(ObjectManager.ObjectManager.Target.Position + new Vector3(0, 0, ObjectManager.ObjectManager.Target.CombatReach / 4)))
                        {
                            if (debug)
                                Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - needInView");
                            return false;
                        }
                    }
                    else
                    {
                        if (TraceLine.TraceLineGo(ObjectManager.ObjectManager.Target.Position))
                        {
                            if (debug)
                                Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - needInView");
                            return false;
                        }
                    }
                }
            }

            if (notSpellIsLuaScript || notSpellIsCSharpCode || notSpellIsVBCode || notSpellIsLuaBotCode)
            {
                bool ret2 = condition(null);
                if (!ret2 && debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - condition");
                return ret2;
            }

            bool ret;
            if (isBuff)
            {
                ret = !spell.HaveBuff &&
                        (!checkIfKnow || spell.KnownSpell) &&
                        (!checkIfSpellUsable || spell.IsSpellUsable);
            }
            else
                ret = (!checkIfKnow || spell.KnownSpell) &&
                      (!checkSpellDistance || targetFriends || spell.IsDistanceGood) &&
                      (!checkIfSpellUsable || spell.IsSpellUsable);

            if (!ret)
            {
                if (debug)
                {
                    if (isBuff)
                        Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - HaveBuff = " + spell.HaveBuff + ", KnownSpell = " + spell.KnownSpell + ", IsSpellUsable = " + spell.IsSpellUsable);
                    else
                        Logging.WriteDebug("[FightClass] " + NameAndDescription +
                                           " - NeedToRun = false - KnownSpell = " + spell.KnownSpell + ", IsDistanceGood = " + spell.IsDistanceGood + " (target distance = " + ObjectManager.ObjectManager.Target.GetDistance + ", max spell distance = " + spell.MaxRange + "), IsSpellUsable = " + spell.IsSpellUsable);
                }
                return false;
            }

            bool condi = condition(null);
            if (!condi && debug)
                Logging.WriteDebug("[FightClass] " + NameAndDescription + " - NeedToRun = false - condition");
            return condi;
        }

        private void RunPrivate()
        {
            MemoryRobot.Int128 currentTargetGUID = ObjectManager.ObjectManager.Target.Guid;
            bool stopMove = true;

            switch (canMoveDuringCast)
            {
                case YesNoAuto.Yes:
                    stopMove = false;
                    break;

                case YesNoAuto.No:
                    stopMove = true;
                    break;

                case YesNoAuto.Auto:
                    if (!notSpellIsLuaScript && !notSpellIsCSharpCode && !notSpellIsVBCode && !notSpellIsLuaBotCode && spell != null && spell.Id > 0)
                        stopMove = spell.CastTime > 0.1f;
                    break;
            }

            if (notSpellIsLuaScript)
            {
                Logging.WriteFight("[FightClass] Launch LUA script: " + spellName);
                if (stopMove)
                    MovementManager.StopMoveTo();
                Lua.LuaDoString(spellName);
                if (!wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting)
                    Usefuls.WaitIsCasting();
                if (isAoE)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Target.Position);
                if (isAoEMePos)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Me.Position);
            }
            else if (notSpellIsCSharpCode)
            {
                Logging.WriteFight("[FightClass] Launch C# code: " + spellName);
                if (stopMove)
                    MovementManager.StopMoveTo();
                RunCodeExtension.RunCsharpScript(spellName);
                if (!wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting)
                    Usefuls.WaitIsCasting();
                if (isAoE)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Target.Position);
                if (isAoEMePos)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Me.Position);
            }
            else if (notSpellIsVBCode)
            {
                Logging.WriteFight("[FightClass] Launch VB.net code: " + spellName);
                if (stopMove)
                    MovementManager.StopMoveTo();
                RunCodeExtension.RunVBScript(spellName);
                if (!wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting)
                    Usefuls.WaitIsCasting();
                if (isAoE)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Target.Position);
                if (isAoEMePos)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Me.Position);
            }
            else if (notSpellIsLuaBotCode)
            {
                Logging.WriteFight("[FightClass] Launch Lua(Bot) code: " + spellName);
                if (stopMove)
                    MovementManager.StopMoveTo();
                RunCodeExtension.RunLuaBotScript(spellName);
                if (!wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting)
                    Usefuls.WaitIsCasting();
                if (isAoE)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Target.Position);
                if (isAoEMePos)
                    ClickOnTerrain.Pulse(ObjectManager.ObjectManager.Me.Position);
            }
            else if (isAoE)
            {
                Logging.WriteFight("[FightClass] Launch AOE Spell: " + spellName);
                ClickOnTerrain.Spell(spell.Id, ObjectManager.ObjectManager.Target.Position, !wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting, stopMove);
            }
            else if (isAoEMePos)
            {
                Logging.WriteFight("[FightClass] Launch AOE Spell (Character position): " + spellName);
                ClickOnTerrain.Spell(spell.Id, ObjectManager.ObjectManager.Me.Position, !wManagerSetting.CurrentSetting.SpellRotationSpeed && waitDuringCasting, stopMove);
            }
            else
            {
                spell.Launch(stopMove, waitDuringCasting && !wManagerSetting.CurrentSetting.SpellRotationSpeed, false, castOn);
            }
            if (timer > 0)
            {
                Timer timerClass = new Timer(timer);
            }

        }

        public override void Run()
        {
            try
            {
                if (!Conditions.InGameAndConnected)
                    return;

                int time = 0;

                if (debug)
                    time = Environment.TickCount;
                RunPrivate();
                if (debug)
                    Logging.WriteDebug("[FightClass] " + NameAndDescription + " - Run() time to execute: " + (Environment.TickCount - time));
            }
            catch (Exception e)
            {
                Logging.WriteError("FightAction>Run: " + e);
            }
        }
        public static void Log(string logString)
        {
            Logging.Write($"[FightAction] {logString}");
            Lua.LuaDoString($@"DEFAULT_CHAT_FRAME:AddMessage(""|cff008000[FightAction]|r :{logString}"")");
        }
        public static void Log(object input)
        {
            
            switch (input ?? "")
            {
                case "":
                    Log("You attempted to log a null value, retard.");
                    break;
                default:
                    Log(input.ToString());
                    break;
            }
        }
        public static void Log(params object[] inputs)
        {
            Log( logString: String.Join(", ", inputs.Select(i => i.ToString())) );
        }
    }
}