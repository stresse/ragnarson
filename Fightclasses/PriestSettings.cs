﻿using robotManager.Helpful;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

[Serializable]
public class PriestSettings : Settings
{
    [Setting]
    [DefaultValue(true)]
    [Category("Buffs")]
    [DisplayName("Power word: Fort")]
    [Description("Enable or disable power word fortitute")]
    public bool EnablePWF { get; set; }

    [Setting]
    [DefaultValue(true)]
    [Category("Combat")]
    [DisplayName("Attack All")]
    [Description("If enabled, bot will attack any mob in its range.")]
    public bool AttackAll { get; set; }

    private PriestSettings()
    {
        EnablePWF = true;
        AttackAll = true;
    }

    public static PriestSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("PriestTutorial", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("PriestSettings > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("PriestTutorial", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<PriestSettings>(AdviserFilePathAndName("PriestTutorial",
                                                                 ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new PriestSettings();
        }
        catch (Exception e)
        {
            Logging.WriteError("PriestSettings > Load(): " + e);
        }
        return false;
    }
}

