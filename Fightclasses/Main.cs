﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using Timer = robotManager.Helpful.Timer;
using robotManager;

public class FightClass : ICustomClass // rename to "Main()"?
{
    private bool _isLaunched;
    public IEnumerable<WoWObject> Corpses = wManager.Wow.ObjectManager.ObjectManager.GetObjectWoWGameObject().Where(o => o.Type == wManager.Wow.Enums.WoWObjectType.Unit && wManager.Wow.Enums.UnitDynamicFlags.Lootable.Equals(o.GetDynamicFlags));
    public string ProjectName = "Priest (Discipline)";
    public List<Spell> Buffs => new List<Spell>() { PowerWordFortitude, };

    //    4 = "Lootable"
    //wManager.Wow.Enums.
    // wManager.Wow.Helpers.;
    public List<Spell> DoTs => new List<Spell>() { ShadowWordPain, };

    public IEnumerable<WoWUnit> Enemies => ObjectManager.GetWoWUnitAttackables().Where(u => u != null && u.IsAlive).OrderBy(u => u.GetDistance);
    public IEnumerable<WoWUnit> EnemiesDefinite => EnemiesInRange.Where(u => u.InCombatWithMe || u.IsTargetingPartyMember || u.IsTargetingMeOrMyPetOrPartyMember);
    public IEnumerable<WoWUnit> EnemiesInLoS => Enemies.Where(u => u.InLoS());
    public IEnumerable<WoWUnit> EnemiesInRange => Enemies.Where(u => u.GetDistance < Range);

    //public IEnumerable<WoWUnit> EnemiesToAttack => Enemies.Where(u => u.IsVillain() && u.InLoS());
    public IEnumerable<WoWUnit> EnemiesToAttack => Enemies.Where(u => u.IsVillainOrJoke() && u.InLoS() && u.GetDistance < Range && !TraceLine.TraceLineGo(ObjectManager.Me.Position, u.Position, hitFlags: CGWorldFrameHitFlags.HitTestLOS));

    public IEnumerable<WoWUnit> EnemiesToMouseover => EnemiesToAttack.Where(u => !u.IsMyTarget);
    public bool InCombat => ObjectManager.Me.InCombat;
    public Spell PowerWordFortitude => new Spell("Power Word: Fortitude");
    public Spell PowerWordSolace => new Spell("Power Word: Solace");
    public float Range { get { return 40f; } }
    public Spell Schism => new Spell("Schism");

    public Spell ShadowWordPain => new Spell("Shadow Word: Pain");

    public Spell Smite => new Spell("Smite");

    private void BuffRotation()
    {
        if (ObjectManager.Me.IsMounted)
            return;

        if (!PowerWordFortitude.HaveBuff && PowerWordFortitude.KnownSpell && PowerWordFortitude.IsSpellUsable && PriestSettings.CurrentSetting.EnablePWF)
        {
            PowerWordFortitude.Launch();
            return;
        }
    }

    private void CombatRotation()
    {
        if (Smite.IsSpellUsable && Smite.IsDistanceGood && !ObjectManager.Target.Rooted)
        {
            Smite.Launch();
            return;
        }
    }

    private void MaintainDebuffs(Spell debuff)
    {
        while (InCombat)
        {
            IEnumerable<WoWUnit> EnemiesToDebuff = EnemiesToAttack.Where(u => !u.HasDebuff(debuff.NameInGame)).OrderBy(u => u.BuffTimeLeft(debuff.NameInGame));
            if (EnemiesToDebuff.Count() > 0)
            {
                WoWUnit target = EnemiesToDebuff.FirstOrDefault();
                Log("Reapplying " + debuff.Name + " on " + target.Name);
                debuff.CastSpell(target);
            }
        }
    }

    public void Dispose()
    {
        _isLaunched = false;
    }

    public void Initialize()
    {
        PriestSettings.Load();
        if (ObjectManager.Me.WowClass == WoWClass.Priest)
        {
            Log("You are a priest! Fight class starting...");
            _isLaunched = true;
            Rotation();
        }
        else
        {
            Log("You are not a priest, change fight class...");
        }
        robotManager.Events.FiniteStateMachineEvents.OnBeforeCheckIfNeedToRunState += (engine, state, cancelable) =>
        {
            if (state != null && state.DisplayName == "Looting")
            {
                if (EnemiesToMouseover.Count() > 0)
                {
                    Logging.Write("Canceling 'Looting' state to kill the rest of our enemies...");
                    cancelable.Cancel = true;
                }
            }
        };
    }

    public void Log(params object[] inputs)
    {
        string logString = "";
        string botLog = @"[{0}] {1}";
        string wowLog = @"DEFAULT_CHAT_FRAME:AddMessage(""|cff008000{0}|r :{1}"")";
        foreach (string str in inputs.Select(s => s.ToString()).ToArray())
        {
            logString += str;
        }
        Logging.Write(botLog.FormatLua(ProjectName, logString));
        Lua.LuaDoString(wowLog.FormatLua(ProjectName, logString));
    }

    /*
        public static IEnumerable<WoWUnit> Enemies

        {
            get
            {
                return ObjectManager.GetWoWUnitAttackables().Where(u => u.IsAlive && !TraceLine.TraceLineGo(ObjectManager.Me.Position, u.Position, CGWorldFrameHitFlags.HitTestLOS) && u.GetDistance < (float)Range).OrderBy(u => u.GetDistance);
            }
        }
     */

    public void Rotation()
    {
        while (_isLaunched)
        {
            try
            {
                if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause)
                {
                    BuffRotation();

                    if (Fight.InFight || ObjectManager.Me.TargetObject.GetDistance < Range || Enemies.Count() > 0 && PriestSettings.CurrentSetting.AttackAll)
                    {
                        //Pull();

                        //Tools.Log("There is a target within range....");
                        CombatRotation();
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError("[My fightclass] ERROR: " + e);
            }

            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }

        Log("Fightclass has reached the end of its loop...so now it's stopping...");
    }

    public void ShowConfiguration()
    {
        PriestSettings.Load();
        PriestSettings.CurrentSetting.ToForm();
        PriestSettings.CurrentSetting.Save();
    }
}