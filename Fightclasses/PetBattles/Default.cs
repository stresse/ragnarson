﻿using System;
using System.Linq;
using System.Threading;
using System.Text;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using Timer = robotManager.Helpful.Timer;
using System.Collections.Generic;
using System;
/*
public class BattlePet
{
	public BattlePet() { }
	public int Index { get; set; }
	public string PetID { get; set; }
	public int SpeciesID { get; set; }
	public bool Owned { get; set; }
	public string CustomName { get; set; }
	public int Level { get; set; }
	public bool Favorite { get; set; }
	public bool IsRevoked { get; set; }
	public string SpeciesName { get; set; }
	public string Icon { get; set; }
	public string PetType { get; set; }
	public int CompanionID { get; set; }
	public string Tooltip { get; set; }
	public string Description { get; set; }
	public bool IsWild { get; set; }
	public bool CanBattle { get; set; }
	public bool IsTradeable { get; set; }
	public bool IsUnique { get; set; }
	public bool Obtainable { get; set; }
	public int Health { get; set; }
	public int MaxHealth { get; set; }
	public int Power { get; set; }
	public int Speed { get; set; }
	public string Rarity { get; set; }
	public BattlePet(int index)
	{
		Index = index;
		string PetInfoFromIndexLUA = string.Format(@"local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex({0}); return PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, _G[""BATTLE_PET_NAME_..i""], CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable", Index);
		List<string> IndexInfo = Lua.LuaDoString<List<string>>(PetInfoFromIndexLUA);
		PetID = IndexInfo[0];
		SpeciesID = Convert.ToInt32(IndexInfo[1]);
		Owned = Convert.ToBoolean(IndexInfo[2]);
		CustomName = IndexInfo[3];
		Level = Convert.ToInt32(IndexInfo[4]);
		Favorite = Convert.ToBoolean(IndexInfo[5]);
		IsRevoked = Convert.ToBoolean(IndexInfo[6]);
		Icon = IndexInfo[7];
		PetType = IndexInfo[8];
		CompanionID = Convert.ToInt32(IndexInfo[9]);
		Tooltip = IndexInfo[10];
		Description = IndexInfo[11];
		IsWild = Convert.ToBoolean(IndexInfo[12]);
		IsTradeable = Convert.ToBoolean(IndexInfo[13]);
		IsUnique = Convert.ToBoolean(IndexInfo[14]);
		Obtainable = Convert.ToBoolean(IndexInfo[15]);
		string PetInfoFromPetIDLUA = string.Format(@"local Health, MaxHealth, Power, Speed, Rarity = C_PetJournal.GetPetStats(""{0}""); return Health MaxHealth, Power, Speed, _G[""BATTLE_PET_BREED_QUALITY""..Rarity]", PetID);
		List<string> PetIDInfo = Lua.LuaDoString<List<string>>(PetInfoFromPetIDLUA);
		Health = Convert.ToInt32(PetIDInfo[0]);
		MaxHealth = Convert.ToInt32(PetIDInfo[1]);
		Power = Convert.ToInt32(PetIDInfo[2]);
		Speed = Convert.ToInt32(PetIDInfo[3]);
		Rarity = PetIDInfo[4];
	}
}
public class PetFinder
{
	static PetFinder() { }
	public static IEnumerable<BattlePet> Pets = Enumerable.Range(1, Lua.LuaDoString<int>("return select(2, C_PetJournal.GetNumPets())")).Select(i => new BattlePet(i));
	public static IEnumerable<BattlePet> FindPetBySpecies(string speciesName)
	{
		return Pets.Where(p => p.SpeciesName == speciesName);
	}
	public static IEnumerable<BattlePet> FindPetBySpecies(int speciesID)
	{
		return Pets.Where(p => p.SpeciesID == speciesID);
	}
	public static IEnumerable<BattlePet> FindReplacements(BattlePet pet)
	{
		IEnumerable<BattlePet> replacements = Pets.Where(p => p.SpeciesID == pet.SpeciesID && p.Level == pet.Level && p.PetID != pet.PetID);
		return replacements;
	}
	public static IEnumerable<BattlePet> FindReplacements(string petID)
	{
		BattlePet pet = Pets.Where(p => p.PetID == petID).FirstOrDefault();
		return FindReplacements(pet);
	}
	public static IEnumerable<BattlePet> FindReplacements(int activePetID)
	{
		string petID;
		try
		{
			petID = Lua.LuaDoString<string>(@"local petGUID, ability1, ability2, ability3, locked = C_PetJournal.GetPetLoadOutInfo(slotIndex)", "petGUID");
		}
		catch (Exception e)
		{
			petID = "";
			Logging.WriteError("FindReplacements(int activePetID): " + e);
			//return FindReplacements(activePetID);
		}
		return FindReplacements(petID);
	}
	
    public IEnumerable<BattlePet> FindReplacement(string petID)
    {
        BattlePet pet = Pets.Where(p => p.PetID == petID);
        FindReplacement(pet);
    }
    

	
}
*/
public class Main : FightBattlePet.IFightClassPet
{
	private const PetBattles.PetFaction Self = PetBattles.PetFaction.Ally;
	private const PetBattles.PetFaction Opponent = PetBattles.PetFaction.Enemy;
	public int turnNum = 0;
	static string PROJECT_NAME = "Stressed PetBattle";
	private const int decoyID = 334;
	int decoyDuration = Lua.LuaDoString<int>(string.Format(@"return C_PetBattles.GetAbilityInfoByID({0});", decoyID));

	public static void Log(bool result)
	{
		Log(result.ToString());
	}
	public static void Log(string text)
	{
		Logging.Write(string.Format("[{0}] {1}", PROJECT_NAME, text));
		Lua.LuaDoString(string.Format("DEFAULT_CHAT_FRAME:AddMessage(\"|cff008000{0}|r :{1}\")", PROJECT_NAME, text));
	}
	public static void tdBattlePetScriptAuto()
	{
		Lua.LuaDoString("tdBattlePetScriptAutoButton:Click()");
	}

	public static List<string> FindPetsByName(string speciesName, int minLevel = 0, int maxLevel = 25)
	{
		string FindPetString = @"local speciesName = '{0}'
		local NUM_PETS = select(2,C_PetJournal.GetNumPets())
		local minLevel = {1}
		local maxLevel = {2}
		local matches = {{}}
		for i=1,NUM_PETS do
			local petID, speciesID, _, _, level, _, _, _speciesName, _, _, _, _, _, _, _, _, _, _ = C_PetJournal.GetPetInfoByIndex(i)
			if (_speciesName == speciesName and level >= minLevel and level <= maxLevel) then
				table.insert(matches,petID)
			end
		end
		return table.concat(matches,'{3}')
		";
		string searchString = FindPetString.FormatLua(speciesName, minLevel, maxLevel, Lua.ListSeparator);
		List<string> PetIDs = Lua.LuaDoString<List<string>>(searchString);
		return PetIDs;
	}

	public void GetName()
	{
		int petIndex = Lua.LuaDoString<int>(string.Format(@"return C_PetBattles.GetActivePet({0});", Self));
		int numAuras = Lua.LuaDoString<int>(string.Format(@"return C_PetBattles.GetNumAuras({0}, {1})", Self, petIndex));
		string.Format("auraID, instanceID, turnsRemaining, isBuff = C_PetBattles.GetAuraInfo({0}, {1}, {2})", Self, petIndex, decoyID);

		Logging.Write("petIndex: " + petIndex.ToString());
	}
	public static int GetBestAbility()
	{
		try
		{
			int activePetSelf = wManager.Wow.Helpers.PetBattles.GetActivePet(Self);
			int activePetOpponent = wManager.Wow.Helpers.PetBattles.GetActivePet(Opponent);
			PetBattles.PetType typePetEnemy = wManager.Wow.Helpers.PetBattles.GetPetType(Opponent, activePetOpponent);

			List<int> bestAbility = new List<int>();
			List<int> weakAbility = new List<int>();
			List<int> normalAbility = new List<int>();
			for (int i = 1; i <= wManager.Wow.Helpers.PetBattles.NUM_BATTLE_PET_ABILITIES; i++)
			{
				if (wManager.Wow.Helpers.PetBattles.GetAbilityState(Self, activePetSelf, i))
				{
					PetBattles.AbilityInfo abilityInfo = wManager.Wow.Helpers.PetBattles.GetAbilityInfo(Self, activePetSelf, i);
					if (!FightBattlePet.AbilitiesBlackListed.Contains(abilityInfo.Name))
					{
						wManager.Wow.Helpers.PetBattles.ModiferEffects abilityModifer =
							wManager.Wow.Helpers.PetBattles.AbilityModifer.Find(effects => effects.Type == abilityInfo.AbilityPetType);
						if (abilityModifer != null)
						{
							if (abilityModifer.Strong == typePetEnemy)
							{
								bestAbility.Add(i);
							}
							if (abilityModifer.Weak == typePetEnemy)
							{
								weakAbility.Add(i);
							}
							else
							{
								normalAbility.Add(i);
							}
						}
					}
				}
			}

			int ability = Others.Random(1, 3);
			if (bestAbility.Count > 0)
			{
				ability = bestAbility[Others.Random(0, bestAbility.Count - 1)];
			}
			else if (normalAbility.Count > 0)
			{
				ability = normalAbility[Others.Random(0, normalAbility.Count - 1)];
			}
			else if (weakAbility.Count > 0)
			{
				ability = weakAbility[Others.Random(0, weakAbility.Count - 1)];
			}
			return ability;
		}
		catch (Exception e)
		{
			Logging.WriteError("GetBestAbility(): " + e);
			return 1;
		}
	}
	public static void UseBestAbility()
	{
		tdBattlePetScriptAuto();
	}

	// Called every 1000 ms in combat
	public void PulseInCombat()
	{
		if (TrapUsable())
		{
			PetBattles.UseTrap();
		}
		tdBattlePetScriptAuto();
		Thread.Sleep(100);
	}

	private Timer _timerAutoOrderPet = new Timer(-1);
	private Timer _timerSpellRevive = new Timer(-1);
	//private int NumPetsOwned = wManager.Wow.Helpers.PetBattles.;

	// Called every 200 ms out of combat
	public void PulseOutOfCombat()
	{
		if (PetBattles.ConditionResurrectBattlePets)
		{
			if (!PetBattles.ReviveBattlePets())
			{
				if (PetBattles.AllPetDead())
				{
					ItemsManager.UseItem(86143); // Battle Pet Bandage
					Usefuls.WaitIsCasting();

					if (PetBattles.AllPetDead() && _timerSpellRevive.IsReady)
					{
						Logging.Write("[FightPetBattle] Revive Battle Pets spell not ready, wait");
						_timerSpellRevive = new Timer(1000 * 20);
					}
				}
			}
		}

		if (FightBattlePet.AutoOrderPetByLevel && _timerAutoOrderPet.IsReady)
		{
			PetBattles.AutoOrderPetInJournalByLevel();
			_timerAutoOrderPet = new Timer(1000 * 60);
		}
		for (int i = 1; i <= wManager.Wow.Helpers.PetBattles.NUM_BATTLE_PETS_IN_BATTLE; i++)
		{
			var replacements = PetFinder.FindReplacements(i);
			Logging.Write(@"Index=" + i + ", NumReplacements=" + replacements.Count().ToString());
			Thread.Sleep(2000);
		}
	}


	private bool TargetTypeIsWeaknesses()
	{
		if (FightBattlePet.AutoChooseBestPet)
		{
			PetBattles.ModiferEffects typeEffects = PetBattles.TypeEffects.Find(effects => effects.Type == PetBattles.GetPetType(Self, PetBattles.GetActivePet(Self)));
			if (typeEffects != null)
			{
				if (typeEffects.Weak == PetBattles.GetPetType(Opponent, PetBattles.GetActivePet(Opponent)))
				{
					return true;
				}
			}
		}
		return false;
	}

	private bool TrapUsable()
	{
		bool conditionsTrap = false;
		if (PetBattles.IsTrapAvailable())
		{
			if (FightBattlePet.CaptureAllPets)
			{
				conditionsTrap = true;
			}
			else if (FightBattlePet.CaptureRarePets)
			{
				PetBattles.PetQuality quality = PetBattles.GetBreedQuality(Opponent, PetBattles.GetActivePet(Opponent));
				if (quality == PetBattles.PetQuality.Rare || quality == PetBattles.PetQuality.Legendary || quality == PetBattles.PetQuality.Epic)
					conditionsTrap = true;
			}
			if (FightBattlePet.CaptureIDontHavePets &&
				conditionsTrap &&
				PetBattles.PetJournalHavePet(PetBattles.GetPetSpeciesID(Opponent, PetBattles.GetActivePet(Opponent))))
			{
				conditionsTrap = false;
			}
		}
		return conditionsTrap;
	}

}

