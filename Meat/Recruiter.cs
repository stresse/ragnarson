using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Plugin;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public class Main : IPlugin
{
    internal const string ProjectName = "Recruiter";
    internal bool Launched;

    public static List<string> Bothered { get; set; }

    private static void Log ( string text )
    {
        Logging.Write ( $"[{ProjectName}] {text}" );
        Lua.LuaDoString ( $"DEFAULT_CHAT_FRAME:AddMessage('|cff008000{ProjectName}|r :{text}')" );
    }

    private static void Log ( int result ) => Log ( result.ToString () );

    private static void Log ( bool result ) => Log ( result.ToString () );

    private static void OpenPetition () => ItemsManager.UseItem ( "Guild Charter" );

    private static void WhisperForGuildCharter ( string playerName ) => Chat.SendChatMessageWhisper ( "Hey mate! I'm trying to set up an alt bank. Mind signing my charter? (Sorry, I know petitions/invitations/etc can be irritating...just need the sig, won't bother you again)" , playerName );

    private void Dispose ()
    {
        Launched = false;
        StopEventHandlers ();
        RecruiterSettings.CurrentSetting.Save ();
    }

    private void LuaEventHandler ( string id , List<string> args )
    {
        switch ( id )
        {
            //case "":;
        }
    }

    private void StartEventHandlers () => EventsLuaWithArgs.OnEventsLuaStringWithArgs += LuaEventHandler;

    private void StopEventHandlers () => EventsLuaWithArgs.OnEventsLuaStringWithArgs -= LuaEventHandler;

    public static bool CheckAlreadyInvited ( string prospectName )
    {
        foreach ( string name in Bothered )
        {
            if ( prospectName == name )
                return true;
        }
        return false;
    }

    public static void InviteToGuild ( string prospectName ) => Lua.LuaDoString ( $"GuildInvite('{prospectName}'" );

    public static void Settings ()
    {
        RecruiterSettings.Load ();
        RecruiterSettings.CurrentSetting.ToForm ();
        RecruiterSettings.CurrentSetting.Save ();
        Log ( "Settings saved." );
    }

    public void GuildRecruiter ()
    {
        while ( Products.IsStarted && Launched )
        {
            List<WoWPlayer> players = wManager.Wow.ObjectManager.ObjectManager.GetObjectWoWPlayer();
            IEnumerable<WoWPlayer> playersQualified = players.Where(p => Lua.LuaDoString() );
            ObjectManager.Me.Target = playersQualified.FirstOrDefault ();
            //WoWPlayer
            while ( Queries.Count > 0 )
            {
                CurrentQuery = Queries.OrderBy ( q => q.BeenRun ).ThenBy ( q => q.LastRun ).First ();
                Log ( CurrentQuery.QueryMessage );
                CurrentQuery.DoWhoQuery ();
                Thread.Sleep ( QueryInterval );
            }
            Log ( "Done with queries!" );
        }
    }

    public void Initialize ()
    {
        StartEventHandlers ();
        Log ( "Initialization completed! New" );
        Launched = true;
    }
}

public class RecruiterSettings : Settings
{
    public RecruiterSettings ()
    {
    }

    public static RecruiterSettings CurrentSetting { get; set; }

    public static bool Load ()
    {
        try
        {
            if ( File.Exists ( AdviserFilePathAndName ( "Recruiter" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) ) )
                CurrentSetting = Load<RecruiterSettings> ( AdviserFilePathAndName ( "Recruiter" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
            else
                CurrentSetting = new RecruiterSettings ();
            return true;
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "RecruiterSettings > Load(): " + e );
            return false;
        }
    }

    public bool Save ()
    {
        try
        {
            return Save ( AdviserFilePathAndName ( "Recruiter" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "RecruiterSettings > Save(): " + e );
            return false;
        }
    }
}