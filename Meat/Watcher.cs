using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;

public class PetBattleFightClass : wManager.Plugin.IPlugin
{
    private const PetBattles.PetFaction Opponent = PetBattles.PetFaction.Enemy;
    private const PetBattles.PetFaction Self = PetBattles.PetFaction.Ally;
    public static bool KillAllRares = Watcher.CurrentSettings.KillAllRaresSetting;
    public static bool KillCritters = Watcher.CurrentSettings.KillAllCrittersSetting;
    public static bool PrioritizeWatchList = Watcher.CurrentSettings.PrioritizeWatchlistSetting;
    public static string ProjectName = "Watcher";
    public bool Launched;
    public bool specialTargetDetected;
    public static bool HaveWatchList { get { return ( WatchList.Count > 0 ) ? true : false; } }

    public static bool InPetBattle { get { return Lua.LuaDoString<bool> ( "return C_PetBattles.IsInBattle()" ); } }
    public static List<string> ItemsToDestroy { get { return Watcher.CurrentSettings.ItemsToDestroyArray.ToList (); } }
    public static List<string> ItemsToUse { get { return Watcher.CurrentSettings.ItemsToUseArray.ToList (); } }
    public static List<string> WatchList { get { return Watcher.CurrentSettings.WatchlistArray.ToList (); } }

    public bool CanFly
    {
        get { return Lua.LuaDoString<bool> ( "return IsFlyableArea()" ); }
    }

    public IEnumerable<WoWObject> Corpses { get { return wManager.Wow.ObjectManager.ObjectManager.GetObjectWoWGameObject ().Where ( o => o.Type == wManager.Wow.Enums.WoWObjectType.Unit && wManager.Wow.Enums.UnitDynamicFlags.Lootable.Equals ( o.GetDynamicFlags ) ); } }
    public IEnumerable<WoWUnit> CrittersToKill { get { return ObjectManager.GetObjectWoWUnit ().Where ( u => u.IsValid && u.IsAlive && u.Name == "Bucktooth Flapper" && u.Entry == 59357 ); } }
    public IEnumerable<WoWUnit> EnemiesToAttack { get { return wManager.Wow.ObjectManager.ObjectManager.GetWoWUnitAttackables ().Where ( u => u != null && u.IsAlive && !TraceLine.TraceLineGo ( ObjectManager.Me.Position , u.Position , wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestLOS ) && ( ( u.Level - ObjectManager.Me.Level >= 20 ) || u.IsTargetingMeOrMyPetOrPartyMember ) ).OrderBy ( u => u.GetDistance ); } }

    public bool HaveHurtPets
    {
        get
        {
            List<int> pets = PetBattles.PetJournalGetExistPetActiveId();
            foreach ( int petId in pets )
            {
                int MaxHealth = PetBattles.GetMaxHealth(PetBattles.PetFaction.Ally, petId);
                int Health = PetBattles.GetHealth(PetBattles.PetFaction.Ally, petId);
                if ( Health < MaxHealth )
                {
                    return true;
                }
            }
            return false;
        }
    }

    public int NearbyCorpseCount { get { return Corpses.Count (); } }

    public int NearbyCrittertCount { get { return NearbyTargets.Count (); } }

    public int NearbyEnemyToAttackCount { get { return EnemiesToAttack.Count (); } }

    public int NearbyRareCount { get { return NearbyRares.Count (); } }

    //    4 = "Lootable"
    public IEnumerable<WoWUnit> NearbyRares { get { return ObjectManager.GetWoWUnitAttackables ().Where ( u => u.IsValid && u.IsAlive && u.UnitClassification.ToString () == "rare" ).OrderBy ( o => ObjectManager.Me.Position.DistanceTo ( o.Position ) ); } }

    public int NearbyTargetCount { get { return NearbyTargets.Count (); } }
    public IEnumerable<WoWUnit> NearbyTargets { get { return ObjectManager.GetWoWUnitByName ( WatchList ).Where ( u => u.IsValid && u.IsAlive ).OrderBy ( o => ObjectManager.Me.Position.DistanceTo ( o.Position ) ); } }

    public bool TrapAppropriate
    {
        get
        {
            bool isAppropriate = false;
            PetBattles.PetQuality quality = PetBattles.GetBreedQuality(PetBattles.PetFaction.Enemy, PetBattles.GetActivePet(PetBattles.PetFaction.Enemy));
            if ( quality == PetBattles.PetQuality.Rare || quality == PetBattles.PetQuality.Legendary || quality == PetBattles.PetQuality.Epic )
            {
                isAppropriate = true;
            }
            return isAppropriate;
        }
    }

    private void FightFix ( WoWUnit unit , CancelEventArgs e )
    {
        // Not in LoS
        if ( TraceLine.TraceLineGo ( unit.Position ) )
        {
            // Get waypoints to the target
            Log ( "Not in Line of Sight." );
            List<Vector3> waypoints = PathFinder.FindPath(unit.Position, true);
            foreach ( Vector3 waypoint in waypoints )
            {
                if ( !TraceLine.TraceLineGo ( waypoint ) )
                {
                    // In LoS
                    //MovementManager.MoveTo(waypoint);
                    // While moving....
                }
                break;
            }
        }
        MemoryRobot.Int128 f = Fight.StartFight(unit.Guid);
    }

    private void LootingEventHandler ( WoWUnit unit )
    {
        foreach ( string itemName in ItemsToDestroy )
        {
            if ( HaveItemNamed ( itemName ) )
            {
                DeleteItemNamed ( itemName );
                Thread.Sleep ( 500 );
            }
        }
        foreach ( string itemName in ItemsToUse )
        {
            if ( HaveItemNamed ( itemName ) )
            {
                UseItemNamed ( itemName );
                Thread.Sleep ( 500 );
            }
        }
    }

    private void PathfindingFix ( Vector3 from , Vector3 to , string continentNameMpq , ConsoleCancelEventArgs cancelable )
    { }

    private void PutSafariHatOn ()
    {
        ItemsManager.UseItem ( "Safari Hat" );
    }

    public static void OnRadar3DDrawEvent ()
    {
        wManager.Wow.Helpers.Radar3D.DrawString ( "This is sample text" , ObjectManager.Me.Position , 14f , System.Drawing.Color.Green , 255 , System.Drawing.FontFamily.GenericSansSerif );
    }

    public void CancelLooting ()
    {
        robotManager.Events.FiniteStateMachineEvents.OnBeforeCheckIfNeedToRunState += ( engine , state , cancelable ) =>
        {
            if ( state.DisplayName == "Looting" )
            {
                cancelable.Cancel = true;
            }
        };
    }

    public void DeleteItemNamed ( string name )
    {
        Log ( string.Format ( "Deleting {0}" , name ) );
        Lua.LuaDoString ( string.Format ( @"for b=0,4 do for s=1, GetContainerNumSlots(b) do local itemLink = GetContainerItemLink(b, s) if itemLink and string.find(itemLink, '{0}') then PickupContainerItem(b, s); DeleteCursorItem(); end end end" , name ) );
    }

    public void DemonHunterGlide ()
    {
        wManager.Events.MovementEvents.OnMovementPulse += ( path , c ) =>
        {
            Vector3 furthestPointInLoS = path.Where(p => !TraceLine.TraceLineGo(ObjectManager.Me.Position, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll)).OrderBy(p => ObjectManager.Me.Position.DistanceTo(p)).LastOrDefault();
            float distance = ObjectManager.Me.Position.DistanceTo(furthestPointInLoS);
            Spell FelRush = new wManager.Wow.Class.Spell("Fel Rush");
            Logging.WriteDebug ( @"Beginning Demon Hunter ""Glide"" ability with a jump" );
            wManager.Wow.Helpers.Move.JumpOrAscend ();
            Thread.Sleep ( 500 );
            Logging.Write ( @"Jump" );
            wManager.Wow.Helpers.Move.JumpOrAscend ();
            Logging.Write ( "NEED SOMETHING HERE" );
            Thread.Sleep ( 500 );
            Logging.Write ( @"Jump" );
            wManager.Wow.Helpers.Move.JumpOrAscend ();
            //Logging.Write("IsGround : " + ObjectManager.Me.IsGround);
            FelRush.Launch ();
            Logging.Write ( @"Done" );
            //        var pointsInLoS = path.Where(p => !TraceLine.TraceLineGo(ObjectManager.Me.Position, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll));
            //		var maxDistance = pointsInLoS.Max(p => ObjectManager.Me.Position.DistanceTo(p));
            //        var furthestPointInLoS = pointsInLoS.

            Logging.Write ( "MaxDistance= " + ObjectManager.Me.Position.DistanceTo ( furthestPointInLoS ) + ", Point = " + furthestPointInLoS.ToString () );
        };
    }

    public void Dispose ()
    {
        // wManager.Events.FightEvents.OnFightStart -= FightFix;
        EventsLuaWithArgs.OnEventsLuaStringWithArgs -= LuaEventHandler;
        wManager.Events.LootingEvents.OnLootSuccessful -= LootingEventHandler;
        wManager.Events.MovementEvents.OnMovementPulse -= MovementHandler;
        wManager.Events.MovementEvents.OnMoveToPulse -= MovementHandler;
        robotManager.Events.FiniteStateMachineEvents.OnBeforeCheckIfNeedToRunState -= OnBeforeCheckIfNeedToRunStateHandler;
        Launched = false;
        Log ( "Disposed." );
    }

    public void DoPetBattle ()
    {
        while ( InPetBattle )
        {
            tdBattlePetScriptAuto ();
            Thread.Sleep ( 500 );
        }
    }

    public void EnableLooting ()
    {
        robotManager.Events.FiniteStateMachineEvents.OnBeforeCheckIfNeedToRunState += ( e , s , c ) =>
        {
            if ( s.DisplayName == "Looting" )
            {
                c.Cancel = false;
            }
        };
    }

    public List<WoWUnit> GetTargetsByName ( List<string> TargetList )
    {
        return ObjectManager.GetWoWUnitByName ( TargetList ).Where ( u => u.IsValid && u.IsAlive ).OrderBy ( o => ObjectManager.Me.Position.DistanceTo ( o.Position ) ).ToList ();
    }

    public bool HaveItemNamed ( string name )
    {
        return ( ItemsManager.GetItemCountByNameLUA ( name ) >= 1 );
    }

    public void Initialize ()
    {
        wManager.Events.LootingEvents.OnLootSuccessful += LootingEventHandler;
        EventsLuaWithArgs.OnEventsLuaStringWithArgs += LuaEventHandler;
        wManager.Events.MovementEvents.OnMovementPulse += MovementHandler;
        wManager.Events.MovementEvents.OnMoveToPulse += MovementHandler;
        robotManager.Events.FiniteStateMachineEvents.OnBeforeCheckIfNeedToRunState += OnBeforeCheckIfNeedToRunStateHandler;
        robotManager.Events.FiniteStateMachineEvents.OnRunState += ( engine , state , cancelable ) =>
        {
            if ( state.DisplayName == "BattlePet" )
            {
                tdBattlePetScriptAuto ();
            }
        };

        Launched = true;
        Radar3D.Pulse ();
        Watcher.Load ();
        Log ( "Initialized." );
        PluginLoop ();
    }

    public void Kill ( IEnumerable<WoWUnit> KillList )
    {
        WoWUnit target = KillList.OrderBy(o => ObjectManager.Me.Position.DistanceTo(o.Position)).ToList().First();
        List<Vector3> path = PathFinder.FindPath(target.Position, true);
        MovementManager.Go ( path );
        if ( target.GetDistance < 35 )
        {
            MemoryRobot.Int128 f = Fight.StartFight(target.Guid, false);
        }
    }

    public void Log ( string text )
    {
        Logging.Write ( string.Format ( "[{0}] {1}" , ProjectName , text ) );
        Lua.LuaDoString ( string.Format ( @"DEFAULT_CHAT_FRAME:AddMessage(""|cff008000{0}|r :{1}"")" , ProjectName , text ) );
    }

    public void Log ( int result )
    {
        Log ( result.ToString () );
    }

    public void Log ( bool result )
    {
        Log ( result.ToString () );
    }

    public void LuaEventHandler ( string id , List<string> args )
    {
        if ( id == "PET_BATTLE_OPENING_DONE" )
        {
            string logMessage = @"Pressing ""A"" because the lua event ""PET_BATTLE_OPENING_DONE"" has fired";
            Logging.WriteDebug ( logMessage );
            tdBattlePetScriptAuto ();           //DoPetBattle() ;
        }
    }

    public void MovementHandler ( List<Vector3> points , CancelEventArgs cancelable )
    {
        if ( ObjectManager.Me.WowClass == wManager.Wow.Enums.WoWClass.DemonHunter && !ObjectManager.Me.IsFlying && !ObjectManager.Me.IsMounted )
        {
            //var pointsInLoS = points.Where(p => !TraceLine.TraceLineGo(ObjectManager.Me.Position, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll));
            //var maxDistance = points.Max(p => ObjectManager.Me.Position.DistanceTo(p));
            //var currentPath = wManager.Wow.Helpers.MovementManager.CurrentPath;
            //var currentPathOrigin = wManager.Wow.Helpers.MovementManager.CurrentPathOrigine;
            Vector3 furthestPointInLoS = points.Where(p => !TraceLine.TraceLineGo(ObjectManager.Me.Position, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll)).OrderBy(p => ObjectManager.Me.Position.DistanceTo(p)).LastOrDefault();
            //wManager.Wow.Helpers.Move.JumpOrAscend;
            MovementManager.MoveTo ( furthestPointInLoS );
        }

        if ( InPetBattle )
        {
            cancelable.Cancel = true;
        }
        if ( ObjectManager.Me.IsFallingFar || ObjectManager.Me.WowClass == wManager.Wow.Enums.WoWClass.DemonHunter && ObjectManager.Me.Position.IsGround () && !ObjectManager.Me.IsMounted && robotManager.Products.Products.ProductName != "Gatherer" )
        {
            TripleJump ();
        }
    }

    public void MovementHandler ( Vector3 point , CancelEventArgs cancelable )
    {
        MovementHandler ( new List<Vector3> { point } , cancelable );
    }

    public void OnBeforeCheckIfNeedToRunStateHandler ( Engine engine , State state , CancelEventArgs cancelable )
    {
        if ( state != null && state.DisplayName == "Looting" && robotManager.Products.Products.ProductName != "Gatherer" )
        {
            if ( NearbyEnemyToAttackCount > 0 && robotManager.Products.Products.ProductName != "Gatherer" && robotManager.Products.Products.ProductName != "Grinder" )
            {
                Log ( @"Canceling 'Looting' state to kill the rest of our enemies..." );
                Logging.WriteDebug ( "Canceling 'Looting' state to kill the rest of our enemies..." );
                cancelable.Cancel = true;
            }
        }
        if ( state.DisplayName == "BattlePet" )
        {
            Log ( @"Canceled ""BattlePet"" State." );
            cancelable.Cancel = true;
            tdBattlePetScriptAuto ();
        }
    }

    public void PluginLoop ()
    {
        while ( Launched )
        {
            if ( InPetBattle )
            {
                DoPetBattle ();
            }
            if ( NearbyTargetCount > 0 )
            {
                Log ( "Killing target." );
                Kill ( NearbyTargets );
            }
            if ( ( NearbyRareCount > 0 ) && ( NearbyRareCount == 0 ) && KillAllRares )
            {
                Log ( "Killing rare." );
                Kill ( NearbyRares );
            }
            if ( NearbyCrittertCount > 0 && KillCritters )
            {
                Log ( "Killing critters..." );
                Kill ( CrittersToKill );
            }
            Thread.Sleep ( 1000 );
        }
    }

    public void PressKey ( string key )
    {
        Keyboard.PressKey ( wManager.Wow.Memory.WowMemory.Memory.WindowHandle , key );
    }

    public void Settings ()
    {
        Watcher.Load ();
        Watcher.CurrentSettings.ToForm ();
        Watcher.CurrentSettings.Save ();
        Log ( "Settings saved." );
    }

    public void tdBattlePetScriptAuto ()
    {
        Lua.LuaDoString ( "tdBattlePetScriptAutoButton:Click()" );
    }

    public void TripleJump ()
    {
        Spell FellRush = new wManager.Wow.Class.Spell("Fel Rush");
        Logging.WriteDebug ( @"Beginning Demon Hunter ""Glide"" ability with a jump" );
        wManager.Wow.Helpers.Move.JumpOrAscend ();
        Thread.Sleep ( 500 );
        Logging.WriteDebug ( @"Jump" );
        wManager.Wow.Helpers.Move.JumpOrAscend ();
        Thread.Sleep ( 300 );
        Logging.WriteDebug ( @"Jump" );
        wManager.Wow.Helpers.Move.JumpOrAscend ();
        Logging.WriteDebug ( @"Done" );
    }

    public void UseItemNamed ( string name )
    {
        if ( HaveItemNamed ( name ) )
        {
            ItemsManager.UseItem ( name );
        }
    }
}

public class Watcher : Settings
{
    public Watcher ()
    {
        KillAllRaresSetting = false;
        KillAllCrittersSetting = false;
        PrioritizeWatchlistSetting = true;
        string[] WatchlistArray = { "", };
        string[] ItemsToDestroyArray = { "", };
        string[] ItemsToUseArray = { "", };
    }

    public static Watcher CurrentSettings { get; set; }

    [Setting]
    [Category ( "Loot Settings" )]
    [DisplayName ( "Items to destroy" )]
    [Description ( "List of items to destroy if they are looted (or in your bags)" )]
    public string[ ] ItemsToDestroyArray { get; set; }

    [Setting]
    [Category ( "Loot Settings" )]
    [DisplayName ( "Items to use" )]
    [Description ( "List of items to use if they are looted (or in your bags)" )]
    public string[ ] ItemsToUseArray { get; set; }

    [Setting]
    [Category ( "Kill Settings" )]
    [DisplayName ( "Kill Critters?" )]
    [Description ( "T/F?" )]
    public bool KillAllCrittersSetting { get; set; }

    [Setting]
    [Category ( "Kill Settings" )]
    [DisplayName ( "Kill All Rare Elites?" )]
    [Description ( "T/F?" )]
    public bool KillAllRaresSetting { get; set; }

    [Setting]
    [Category ( "Kill Settings" )]
    [DisplayName ( "Prioritize Watch List" )]
    [Description ( @"If both options are selected, should the ""Watch List"" take priority over the rare elites?" )]
    public bool PrioritizeWatchlistSetting { get; set; }

    [Setting]
    [Category ( "Kill Settings" )]
    [DisplayName ( "Watch List" )]
    [Description ( "Which units should we be watching for?" )]
    public string[ ] WatchlistArray { get; set; }

    public static bool Load ()
    {
        try
        {
            if ( File.Exists ( AdviserFilePathAndName ( "Watcher" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) ) )
            {
                CurrentSettings = Load<Watcher> ( AdviserFilePathAndName ( "Watcher" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
                return true;
            }
            CurrentSettings = new Watcher ();
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "Watcher > Load(): " + e );
        }
        return false;
    }

    public bool Save ()
    {
        try
        {
            return Save ( AdviserFilePathAndName ( "Watcher" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "Watcher > Save(): " + e );
            return false;
        }
    }
}

/*
namespace WowUI
{
	public class Frames
    {
		string createFrameString = @"local frame = CreateFrame(""{0}"", ""{1}"", {2}, ""{3}"", {4})" ; //	frameRef, frameType, frameName, parentFrame, inheritsFrame, id
		static void CreateFrame(string frameRef, string frameType, string frameName, string parentFrame, string inheritsFrame, int id)
		{
			Lua.LuaDoString(createFrameString.Format(frameRef, frameType, frameName, parentFrame, inheritsFrame, id));
		}
	}
}
*/