using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;

using System.Threading;
using robotManager.FiniteStateMachine;

public class BattlePet
{
    public BattlePet ()
    {
    }

    public BattlePet ( List<string> result )
    {
        int Index = Convert.ToInt32(result[0]);
        string PetID = result[1];
        int SpeciesID = Convert.ToInt32(result[2]);
        bool Owned = Convert.ToBoolean(result[3]);
        string CustomName = result[4];
        int Level = Convert.ToInt32(result[5]);
        bool Favorite = Convert.ToBoolean(result[6]);
        bool IsRevoked = Convert.ToBoolean(result[7]);
        string SpeciesName = result[8];
        string Icon = result[9];
        string PetType = result[10];
        int CompanionID = Convert.ToInt32(result[11]);
        string Tooltip = result[12];
        string Description = result[13];
        bool IsWild = Convert.ToBoolean(result[14]);
        bool CanBattle = Convert.ToBoolean(result[15]);
        bool IsTradeable = Convert.ToBoolean(result[16]);
        bool IsUnique = Convert.ToBoolean(result[17]);
        bool Obtainable = Convert.ToBoolean(result[18]);
        int Health = Convert.ToInt32(result[19]);
        int MaxHealth = Convert.ToInt32(result[20]);
        int Power = Convert.ToInt32(result[21]);
        int Speed = Convert.ToInt32(result[22]);
        string Rarity = result[23];
    }

    public BattlePet ( int index , string petID , int speciesID , bool owned , string customName , int level , bool favorite , bool isRevoked , string speciesName , string icon , string petType , int companionID , string tooltip , string description , bool isWild , bool canBattle , bool isTradeable , bool isUnique , bool obtainable , int health , int maxHealth , int power , int speed , string rarity )
    {
        int Index = index;
        string PetID = petID;
        int SpeciesID = Convert.ToInt32(speciesID);
        bool Owned = Convert.ToBoolean(owned);
        string CustomName = customName;
        int Level = Convert.ToInt32(level);
        bool Favorite = Convert.ToBoolean(favorite);
        bool IsRevoked = Convert.ToBoolean(isRevoked);
        string SpeciesName = speciesName;
        string Icon = icon;
        string PetType = petType;
        int CompanionID = Convert.ToInt32(companionID);
        string Tooltip = tooltip;
        string Description = description;
        bool IsWild = Convert.ToBoolean(isWild);
        bool CanBattle = Convert.ToBoolean(canBattle);
        bool IsTradeable = Convert.ToBoolean(isTradeable);
        bool IsUnique = Convert.ToBoolean(isUnique);
        bool Obtainable = Convert.ToBoolean(obtainable);
        int Health = Convert.ToInt32(health);
        int MaxHealth = Convert.ToInt32(maxHealth);
        int Power = Convert.ToInt32(power);
        int Speed = Convert.ToInt32(speed);
        string Rarity = rarity;
    }

    public BattlePet ( int index )
    {
        string PetInfoFromIndexLUA = string.Format(@"local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex({0}); return PetID, SpeciesID, Owned, CustomName or """", Level, Favorite, IsRevoked, SpeciesName, Icon, _G[""BATTLE_PET_NAME_""..PetType], CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable", index);
        List<string> IndexInfo = Lua.LuaDoString<List<string>>(PetInfoFromIndexLUA);
        int Index = index;
        string PetID = IndexInfo[0];
        int SpeciesID = Convert.ToInt32(IndexInfo[1]);
        bool Owned = Convert.ToBoolean(IndexInfo[2]);
        string CustomName = IndexInfo[3];
        int Level = Convert.ToInt32(IndexInfo[4]);
        bool Favorite = Convert.ToBoolean(IndexInfo[5]);
        bool IsRevoked = Convert.ToBoolean(IndexInfo[6]);
        string SpeciesName = IndexInfo[7];
        string Icon = IndexInfo[8];
        string PetType = IndexInfo[9];
        int CompanionID = Convert.ToInt32(IndexInfo[10]);
        string Tooltip = IndexInfo[11];
        string Description = IndexInfo[12];
        bool IsWild = Convert.ToBoolean(IndexInfo[13]);
        bool CanBattle = Convert.ToBoolean(IndexInfo[14]);
        bool IsTradeable = Convert.ToBoolean(IndexInfo[15]);
        bool IsUnique = Convert.ToBoolean(IndexInfo[16]);
        bool Obtainable = Convert.ToBoolean(IndexInfo[17]);
        /*
        Index = index;
        string PetInfoFromIndexLUA = string.Format(@"local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex({0}); return PetID, SpeciesID, Owned, CustomName or """", Level, Favorite, IsRevoked, SpeciesName, Icon, _G[""BATTLE_PET_NAME_""..PetType], CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable", Index);
        List<string> IndexInfo = Lua.LuaDoString< List<string> > ( PetInfoFromIndexLUA ) ;
        PetID = IndexInfo[0];
        SpeciesID = Convert.ToInt32(IndexInfo[1]);
        Owned = Convert.ToBoolean(IndexInfo[2]);
        CustomName = IndexInfo[3];
        Level = Convert.ToInt32(IndexInfo[4]);
        Favorite = Convert.ToBoolean(IndexInfo[5]);
        IsRevoked = Convert.ToBoolean(IndexInfo[6]);
        SpeciesName = IndexInfo[7];
        Icon = IndexInfo[8];
        PetType = IndexInfo[9];
        CompanionID = Convert.ToInt32(IndexInfo[10]);
        Tooltip = IndexInfo[11];
        Description = IndexInfo[12];
        IsWild = Convert.ToBoolean(IndexInfo[13]);
        CanBattle = Convert.ToBoolean(IndexInfo[14]);
        IsTradeable = Convert.ToBoolean(IndexInfo[15]);
        IsUnique = Convert.ToBoolean(IndexInfo[16]);
        Obtainable = Convert.ToBoolean(IndexInfo[17]);
        */
    }

    public bool CanBattle { get; set; }
    public int CompanionID { get; set; }
    public string CustomName { get; set; }
    public string Description { get; set; }
    public bool Favorite { get; set; }
    public int Health { get; set; }
    public string Icon { get; set; }
    public int Index { get; set; }
    public bool IsRevoked { get; set; }
    public bool IsTradeable { get; set; }
    public bool IsUnique { get; set; }
    public bool IsWild { get; set; }
    public int Level { get; set; }
    public int MaxHealth { get; set; }
    public bool Obtainable { get; set; }
    public bool Owned { get; set; }
    public string PetID { get; set; }
    public string PetType { get; set; }
    public int Power { get; set; }
    public string Rarity { get; set; }
    public int SpeciesID { get; set; }
    public string SpeciesName { get; set; }
    public int Speed { get; set; }
    public string Tooltip { get; set; }
    //public bool NeedsHealing { get; set; }
}

public class Main : wManager.Plugin.IPlugin
{
    private const PetBattles.PetFaction Opponent = PetBattles.PetFaction.Enemy;
    private const PetBattles.PetFaction Self = PetBattles.PetFaction.Ally;
    private bool Launched;
    public static string ProjectName = "PetMinder";
    public static bool InPetBattle { get { return Lua.LuaDoString<bool> ( "return C_PetBattles.IsInBattle()" ); } }

    public bool HaveHurtPets
    {
        get
        {
            List<int> pets = PetBattles.PetJournalGetExistPetActiveId();
            foreach ( int petId in pets )
            {
                int MaxHealth = PetBattles.GetMaxHealth(PetBattles.PetFaction.Ally, petId);
                int Health = PetBattles.GetHealth(PetBattles.PetFaction.Ally, petId);
                if ( Health < MaxHealth )
                {
                    return true;
                }
            }
            return false;
        }
    }

    public bool TrapAppropriate
    {
        get
        {
            bool isAppropriate = false;
            PetBattles.PetQuality quality = PetBattles.GetBreedQuality(PetBattles.PetFaction.Enemy, PetBattles.GetActivePet(PetBattles.PetFaction.Enemy));
            if ( quality == PetBattles.PetQuality.Rare || quality == PetBattles.PetQuality.Legendary || quality == PetBattles.PetQuality.Epic )
            {
                isAppropriate = true;
            }
            return isAppropriate;
        }
    }

    private void PutSafariHatOn ()
    {
        ItemsManager.UseItem ( "Safari Hat" );
    }

    public void Dispose ()
    {
        Log ( "Disposed." );
    }

    public void Initialize ()
    {
        Launched = true;
        PetMinderSettings.Load ();
        CVar.SetCVar("autointeract", 1);
        Log ( "Initialized." );
        PluginLoop ();
    }

    public void Log ( string text )
    {
        Logging.Write ( string.Format ( "[{0}] {1}" , ProjectName , text ) );
        Lua.LuaDoString ( string.Format ( @"DEFAULT_CHAT_FRAME:AddMessage(""|cff008000{0}|r :{1}"")" , ProjectName , text ) );
    }

    public void Log ( int result )
    {
        Log ( result.ToString () );
    }

    public void Log ( bool result )
    {
        Log ( result.ToString () );
    }

    public void PluginLoop ()
    {
        Log ( "Loop started." );
        var pets = PetFinder.FindReplacements(2);
        Log ( "Pet found" );
        Logging.Write ( pets.Count ().ToString () );
        Log ( "Loop ended" );
    }

    public void Settings ()
    {
        PetMinderSettings.Load ();
        PetMinderSettings.CurrentSettings.ToForm ();
        PetMinderSettings.CurrentSettings.Save ();
        Log ( "Settings saved." );
    }

    public void tdBattlePetScriptAuto ()
    {
        Lua.RunMacroText ( "/click tdBattlePetScriptAutoButton" );
    }

    /*
    public static FindPets(string speciesName, int MinLevel, int MaxLevel,  )
        string queryString = @"
local matches = {{}}
local numPets, numOwned = C_PetJournal.GetNumPets()

for i = 1, numOwned do
   local Index = i
   local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex(Index); CustomName = CustomName or """"; PetType = _G[""BATTLE_PET_NAME_""..PetType];
   local Health, MaxHealth, Power, Speed, Rarity = C_PetJournal.GetPetStats(PetID)
   DEFAULT_CHAT_FRAME:AddMessage(PetID)
   if ({CONDITION}) then
      DEFAULT_CHAT_FRAME:AddMessage(PetID)
      table.insert(matches, PetID)
   end
end
return table.concat(matches, {})";
*/
}

public class PetFinder
{
    public static List<BattlePet> AllPets = new List<BattlePet> { };
    int NUM_PETS_OWNED => Lua.LuaDoString<int> ( "local numPets, numOwned = C_PetJournal.GetNumPets(); return numOwned ;" );

    private static void Dispose ()
    {
        Logging.Write ( "Petfinder disposed" );
    }

    /*
    public static BattlePet GetBattlePetFromIndex(int index)
    {
        string queryString = @"
local Index = " + i + @"
local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex(Index);
local Health, MaxHealth, Power, Speed, Rarity = C_PetJournal.GetPetStats(PetID);
local infoStringTable = {Index, PetID, SpeciesID, tostring(Owned), CustomName or """", Level, tostring(Favorite), tostring(IsRevoked), SpeciesName, Icon, _G['BATTLE_PET_NAME_'..PetType], CompanionID, Tooltip, Description, tostring(IsWild), tostring(CanBattle), tostring(IsTradeable), tostring(IsUnique), tostring(Obtainable), Health, MaxHealth, Power, Speed, _G['BATTLE_PET_BREED_QUALITY'..Rarity]}
return table.concat( infoStringTable, '" + Lua.ListSeparator + "' );";
        var infoList = Lua.LuaDoString<List<string>>(queryString);
        return new BattlePet(infoList);
    }
    */

    public static void Initialize ()
    {
        Logging.Write ( "Petfinder Initialized" );
        int NUM_PETS_OWNED = Lua.LuaDoString<int>(@"local numPets, numOwned = C_PetJournal.GetNumPets(); return numOwned;");

        string queryString = @"
                            local matches = {}
                            local numPets, numOwned = C_PetJournal.GetNumPets()

                            for i = 1, numOwned do
                               local Index = i
                               local PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable = C_PetJournal.GetPetInfoByIndex(Index);  CustomName = CustomName or ''; PetType = _G['BATTLE_PET_NAME_'..PetType];
                               local Health, MaxHealth, Power, Speed, Rarity = C_PetJournal.GetPetStats(PetID); Rarity = _G['BATTLE_PET_BREED_QUALITY'..Rarity];
                               local infoStringTable = {Index, PetID, SpeciesID, tostring(Owned) or '', CustomName or '', Level, tostring(Favorite), tostring(IsRevoked), SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, tostring(IsWild), tostring(CanBattle), tostring(IsTradeable), tostring(IsUnique), tostring(Obtainable), Health, MaxHealth, Power, Speed, Rarity}
                               local infoString = table.concat( infoStringTable, '#||#' )
                               print(infoString)
                               table.insert(matches, infoString)
                            end
                            return unpack(matches)";

        List<string> result = Lua.LuaDoString<List<string>>(queryString);
        //var workingResult = (result.Count % 24);

        //Logging.Write("Number Of Results : " + result.Count + "Value : " + result[0]);
        Logging.Write ( "Number Of Results : " + result.Count );
        //int petIndex = 0;
        int numResults = result.Count;
        for ( int petIndex = 0; petIndex + 24 <= numResults; petIndex += 24 )
        {
            //Logging.Write(result[petIndex]);
            int Index = Convert.ToInt32(result[petIndex]);
            string PetID = result[petIndex + 1];
            int SpeciesID = Convert.ToInt32(result[petIndex + 2]);
            bool Owned = Convert.ToBoolean(result[petIndex + 3]);
            string CustomName = result[petIndex + 4];
            int Level = Convert.ToInt32(result[petIndex + 5]);
            bool Favorite = Convert.ToBoolean(result[petIndex + 6]);
            bool IsRevoked = Convert.ToBoolean(result[petIndex + 7]);
            string SpeciesName = result[petIndex + 8];
            string Icon = result[petIndex + 9];
            string PetType = result[petIndex + 10];
            int CompanionID = Convert.ToInt32(result[petIndex + 11]);
            string Tooltip = result[petIndex + 12];
            string Description = result[petIndex + 13];
            bool IsWild = Convert.ToBoolean(result[petIndex + 14]);
            bool CanBattle = Convert.ToBoolean(result[petIndex + 15]);
            bool IsTradeable = Convert.ToBoolean(result[petIndex + 16]);
            bool IsUnique = Convert.ToBoolean(result[petIndex + 17]);
            bool Obtainable = Convert.ToBoolean(result[petIndex + 18]);
            int Health = Convert.ToInt32(result[petIndex + 19]);
            int MaxHealth = Convert.ToInt32(result[petIndex + 20]);
            int Power = Convert.ToInt32(result[petIndex + 21]);
            int Speed = Convert.ToInt32(result[petIndex + 22]);
            string Rarity = result[petIndex + 23];
            BattlePet Pet = new BattlePet(Index, PetID, SpeciesID, Owned, CustomName, Level, Favorite, IsRevoked, SpeciesName, Icon, PetType, CompanionID, Tooltip, Description, IsWild, CanBattle, IsTradeable, IsUnique, Obtainable, Health, MaxHealth, Power, Speed, Rarity);
            AllPets.Add ( Pet );
        }
        //Pets = Enumerable.Range(1, NUM_PETS_OWNED).Select(i => new BattlePet(i));
    }

    /*
    public static IEnumerable<BattlePet> Pets = Enumerable.Range(1, NUM_PETS_OWNED).Select(i => new BattlePet(i));

    public static int GetSpeciesID(string speciesName)
    {
        return Lua.LuaDoString<int>(@"local speciesId, petGUID = C_PetJournal.FindPetIDByName(" + speciesName + ") ; return speciesId ;");
    }
    public static IEnumerable<BattlePet> FindPetBySpecies(string speciesName)
    {
        return Pets.Where(p => p.SpeciesName == speciesName);
    }
    public static IEnumerable<BattlePet> FindPetBySpecies(int speciesID)
    {
        return Pets.Where(p => p.SpeciesID == speciesID);
    }
    public static IEnumerable<BattlePet> FindReplacements(BattlePet pet)
    {
        IEnumerable<BattlePet> replacements = Pets.Where(p => p.SpeciesID == pet.SpeciesID && p.Level == pet.Level && p.PetID != pet.PetID);
        return replacements;
    }
    public static IEnumerable<BattlePet> FindReplacements(string petID)
    {
        BattlePet pet = Pets.Where(p => p.PetID == petID).FirstOrDefault();
        return FindReplacements(pet);
    }
    public static IEnumerable<BattlePet> FindReplacements(int activePetID)
    {
        string petID = "";
        try
        {
            petID = Lua.LuaDoString<string>(@"local petGUID, ability1, ability2, ability3, locked = C_PetJournal.GetPetLoadOutInfo(" + activePetID + ") ; return petGUID ;");
        }
        catch (Exception e)
        {
            Logging.WriteError("FindReplacements (int activePetID) : " + e);
        }
        return FindReplacements(petID);
    }
    */
}

public class PetMinderSettings : Settings
{
    public PetMinderSettings ()
    {
        CapturePetsSetting = true;
    }

    public static PetMinderSettings CurrentSettings { get; set; }

    [Setting]
    [Category ( "Should we capture all pets?" )]
    [DisplayName ( "Capture?" )]
    [Description ( "T/F?" )]
    public bool CapturePetsSetting { get; set; }

    public static bool Load ()
    {
        try
        {
            if ( File.Exists ( AdviserFilePathAndName ( "PetMinder" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) ) )
            {
                CurrentSettings = Load<PetMinderSettings> ( AdviserFilePathAndName ( "PetMinder" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
                return true;
            }
            CurrentSettings = new PetMinderSettings ();
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "Watcher > Load(): " + e );
        }
        return false;
    }

    public bool Save ()
    {
        try
        {
            return Save ( AdviserFilePathAndName ( "PetMinder" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
        }
        catch ( Exception e )
        {
            Logging.WriteError ( "PetMinder > Save(): " + e );
            return false;
        }
    }
}

/*
public static IEnumerable<BattlePet> FindReplacements(BattlePet pet)
{
    IEnumerable<BattlePet> replacements = Pets.Where(p => p.SpeciesID == pet.SpeciesID && p.Level == pet.Level && p.PetID != pet.PetID);
    return replacements;
}
public static IEnumerable<BattlePet> FindReplacements(string petID)
{
    BattlePet pet = Pets.Where(p => p.PetID == petID).FirstOrDefault();
    return FindReplacements(pet);
}
public static IEnumerable<BattlePet> FindReplacements(int activePetID)
{
    string petID;
    try
    {
        petID = Lua.LuaDoString<string>(@"local petGUID, ability1, ability2, ability3, locked = C_PetJournal.GetPetLoadOutInfo(slotIndex); return petGUID;");
    }
    catch (Exception e)
    {
        petID = "";
        Logging.WriteError("FindReplacements(int activePetID): " + e);
        //return FindReplacements(activePetID);
    }
    return FindReplacements(petID);
}
*/