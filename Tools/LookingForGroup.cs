﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using Timer = robotManager.Helpful.Timer;
using robotManager;
using Constants;

public class LookingForGroup
{
    static LookingForGroup() { }
    public string DungeonFinderButton = "GroupFinderFrameGroupButton1";
    public string RaidFinderButton = "GroupFinderFrameGroupButton3";
    public string PremadeGroupButton = "GroupFinderFrameGroupButton4";
    //public string DungeonFinderDropdownMenuName

    public bool Launched { get; set; }
    public bool CurrentlyInInstance { get { return Lua.LuaDoString<bool>("return select(1,IsInInstance())"); ; } }
    public void Click(string buttonName)
    {
        Lua.LuaDoString(buttonName + ":Click()");
    }
    public void ToggleLFG()
    {
        Click("LFDMicroButton");
    }
    public void ToggleDungeonFinder()
    {
        ToggleLFG();
        Click(DungeonFinderButton);
    }
    public void ToggleRaidFinder()
    {
        ToggleLFG();
        Click(RaidFinderButton);
    }
    public void TogglePremadeGroups()
    {
        ToggleLFG();
        Click(PremadeGroupButton);
    }
    public void SelectGFDDropdownOption(string option)
    {
        Click(Constants.Buttons.DungeonFinder.DropdownMenu);
        Click(buttonName: Constants.Buttons.Dropdown.DropdownOptionN.FormatLua(option));

    }
    public void SelectGFDDropdownOption(int option)
    {
        SelectGFDDropdownOption(option.ToString());
    }
    public void QueueDungeonFinderOption(string option)
    {
        ToggleDungeonFinder();
        Click(Constants.Buttons.DungeonFinder.DropdownMenu);
        Click(Constants.Buttons.Dropdown.DropdownOptionN.FormatLua(option));
        Click(Constants.Buttons.DungeonFinder.FindGroup);
    }
    public void QueueDungeonFinderOption(int option)
    {
        QueueDungeonFinderOption(option.ToString());
    }
    public void LeaveInstanceGroup()
    {
        Click(Constants.Buttons.Minimap.QueueStatusDropdown);
        Click(Constants.Buttons.Dropdown.DropdownOption3);
    }
    public void AcceptGroupPopUp()
    {
        Click(Constants.Buttons.PopUp.LFD.EnterDungeon);
    }
    public void Loop()
    {
        while (Launched)
        {
            if (!CurrentlyInInstance)
            {
                SelectGFDDropdownOption(2);
            }
        }

    }

}
